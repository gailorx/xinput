#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use types::{GUID, BOOL, DWORD, UINT, BYTE, LPWSTR};
use super::{XINPUT_STATE, XINPUT_VIBRATION, XINPUT_CAPABILITIES, XINPUT_BATTERY_INFORMATION, PXINPUT_KEYSTROKE};

pub const XINPUT_DLL_1_4: &'static str = "xinput1_4.dll";
pub const XINPUT_DLL_1_3: &'static str = "xinput9_1_0.dll";
pub const XINPUT_DLL_9_1_0: &'static str = "xinput9_1_0.dll";

pub type XInputEnableFuncPtr = unsafe extern "stdcall" fn(BOOL);
pub type XInputGetAudioDeviceIdsFuncPtr = unsafe extern "stdcall" fn(DWORD, LPWSTR, *mut UINT, LPWSTR, *mut UINT) -> DWORD;
pub type XInputGetBatteryInformationFuncPtr = unsafe extern "stdcall" fn(DWORD, BYTE, *mut XINPUT_BATTERY_INFORMATION) -> DWORD;
pub type XInputGetCapabilitiesFuncPtr = unsafe extern "stdcall" fn(DWORD, DWORD, *mut XINPUT_CAPABILITIES) -> DWORD;
pub type XInputGetDSoundAudioDeviceGuidsFuncPtr = unsafe extern "stdcall" fn(DWORD, *mut GUID, *mut GUID) -> DWORD;
pub type XInputGetKeystrokeFuncPtr = unsafe extern "stdcall" fn(DWORD, DWORD, PXINPUT_KEYSTROKE) -> DWORD;
pub type XInputGetStateFuncPtr = unsafe extern "stdcall" fn(DWORD, *mut XINPUT_STATE) -> DWORD;
pub type XInputSetStateFuncPtr = unsafe extern "stdcall" fn(DWORD, *mut XINPUT_VIBRATION) -> DWORD;