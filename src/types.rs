#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

use libc::{c_ulong, c_ushort, c_uchar};
pub use win32_types::{LONG, DWORD, WORD, UINT, SHORT, BYTE, WCHAR, LPWSTR, BOOL};

#[repr(C)]
pub struct GUID {
	pub Data1: c_ulong,
	pub Data2: c_ushort,
	pub Data3: c_ushort,
	pub Data4: [c_uchar; 8],
}
impl Copy for GUID {}