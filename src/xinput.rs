#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]

use types::{GUID, DWORD, WORD, UINT, SHORT, BYTE, WCHAR, LPWSTR, BOOL};

pub mod ERROR {
	use types::LONG;
	// MessageId:
	// ERROR_SUCCESS
	// MessageText:
	// The operation completed successfully.
	pub const SUCCESS: LONG = 0;
	// MessageId:
	// ERROR_DEVICE_NOT_CONNECTED
	// MessageText:
	// The device is not connected.
	pub const DEVICE_NOT_CONNECTED: LONG = 1167;
	// MessageId:
	// ERROR_EMPTY
	// MessageText:
	// The library, drive, or media pool is empty.
	pub const EMPTY: LONG = 4306;
}

pub const XUSER_MAX_COUNT: DWORD = 4;
pub const XUSER_INDEX_ANY: DWORD = 0x000000FF;

#[repr(C)]
pub struct XINPUT_BATTERY_INFORMATION {
	pub BatteryType: BYTE,
	pub BatteryLevel: BYTE,
}
impl Copy for XINPUT_BATTERY_INFORMATION {}
pub type PXINPUT_BATTERY_INFORMATION = *mut XINPUT_BATTERY_INFORMATION;

pub mod BATTERY_DEVTYPE {
	use types::DWORD;

	pub const GAMEPAD: DWORD = 0x00;
	pub const HEADSET: DWORD = 0x01;
}

pub mod BATTERY_TYPE {
	use types::DWORD;

	pub const DISCONNECTED: DWORD = 0x00;
	pub const WIRED: DWORD = 0x01;
	pub const ALKALINE: DWORD = 0x02;
	pub const NIMH: DWORD = 0x03;
	pub const UNKNOWN: DWORD = 0xFF;
}

pub mod BATTERY_LEVEL {
	use types::DWORD;

	pub const EMPTY: DWORD = 0x00;
	pub const LOW: DWORD = 0x01;
	pub const MEDIUM: DWORD = 0x02;
	pub const FULL: DWORD = 0x03;
}

#[repr(C)]
pub struct XINPUT_CAPABILITIES {
	pub Type: BYTE,
	pub SubType: BYTE,
	pub Flags: WORD,
	pub Gamepad: XINPUT_GAMEPAD,
	pub Vibration: XINPUT_VIBRATION,
}
impl Copy for XINPUT_CAPABILITIES {}
pub type PXINPUT_CAPABILITIES = *mut XINPUT_CAPABILITIES;

pub mod XINPUT_DEVTYPE {
	use types::DWORD;

	pub const GAMEPAD: DWORD = 0x01;
}

pub mod XINPUT_DEVSUBTYPE {
	use types::DWORD;

	pub const UNKNOWN: DWORD = 0x00;	//Unknown. The controller type is unknown.
	pub const GAMEPAD: DWORD = 0x01; //Gamepad controller. Includes Left and Right Sticks, Left and Right Triggers, Directional Pad, and all standard buttons (A, B, X, Y, START, BACK, LB, RB, LSB, RSB).
	pub const WHEEL: DWORD = 0x02;	//Racing wheel controller. Left Stick X reports the wheel rotation, Right Trigger is the acceleration pedal, and Left Trigger is the brake pedal. Includes Directional Pad and most standard buttons (A, B, X, Y, START, BACK, LB, RB). LSB and RSB are optional.
	pub const ARCADE_STICK: DWORD = 0x03;	//Arcade stick controller. Includes a Digital Stick that reports as a DPAD (up, down, left, right), and most standard buttons (A, B, X, Y, START, BACK). The Left and Right Triggers are implemented as digital buttons and report either 0 or 0xFF. LB, LSB, RB, and RSB are optional.
	pub const FLIGHT_STICK: DWORD = 0x04;	//Flight stick controller. Includes a pitch and roll stick that reports as the Left Stick, a POV Hat which reports as the Right Stick, a rudder (handle twist or rocker) that reports as Left Trigger, and a throttle control as the Right Trigger. Includes support for a primary weapon (A), secondary weapon (B), and other standard buttons (X, Y, START, BACK). LB, LSB, RB, and RSB are optional.
	pub const DANCE_PAD: DWORD = 0x05;	//Dance pad controller. Includes the Directional Pad and standard buttons (A, B, X, Y) on the pad, plus BACK and START.

	pub const GUITAR: DWORD = 0x06; //Guitar controller. The strum bar maps to DPAD (up and down), and the frets are assigned to A (green), B (red), Y (yellow), X (blue), and LB (orange). Right Stick Y is associated with a vertical orientation sensor, Right Stick X is the whammy bar. Includes support for BACK, START, DPAD (left, right). Left Trigger (pickup selector), Right Trigger, RB, LSB (fret modifier), RSB are optional.
	pub const GUITAR_ALTERNATE: DWORD = 0x07; // Guitar Alt supports a larger range of movement for the vertical orientation sensor.
	pub const GUITAR_BASS: DWORD = 0x0B; // Guitar Bass is identical to Guitar, with the distinct subtype to simplify setup.

	pub const DRUM_KIT: DWORD = 0x08;	//Drum controller. The drum pads are assigned to buttons: A for green (Floor Tom), B for red (Snare Drum), X for blue (Low Tom), Y for yellow (High Tom), and LB for the pedal (Bass Drum). Includes Directional-Pad, BACK, and START. RB, LSB, and RSB are optional.
	pub const ARCADE_PAD: DWORD = 0x13; //Arcade pad controller. Includes Directional Pad and most standard buttons (A, B, X, Y, START, BACK, LB, RB). The Left and Right Triggers are implemented as digital buttons and report either 0 or 0xFF. Left Stick, Right Stick, LSB, and RSB are optional.
}

pub mod XINPUT_CAPS {
	use types::DWORD;

	pub const VOICE_SUPPORTED: DWORD = 0x0004; // Device has an integrated voice device.
	pub const FFB_SUPPORTED: DWORD = 0x0001; // Device supports force feedback functionality. Note that these force-feedback features beyond rumble are not currently supported through XINPUT on Windows.
	pub const WIRELESS: DWORD = 0x0002; // Device is wireless.
	pub const PMD_SUPPORTED: DWORD = 0x0008;	// Device supports plug-in modules. Note that plug-in modules like the text input device (TID) are not supported currently through XINPUT on Windows.
	pub const NO_NAVIGATION: DWORD = 0x0010;	// Device lacks menu navigation buttons (START, BACK, DPAD).
}

#[repr(C)]
pub struct XINPUT_GAMEPAD {
	pub wButtons: WORD,
	pub bLeftTrigger: BYTE,
	pub bRightTrigger: BYTE,
	pub sThumbLX: SHORT,
	pub sThumbLY: SHORT,
	pub sThumbRX: SHORT,
	pub sThumbRY: SHORT,
}
impl Copy for XINPUT_GAMEPAD {}
pub type PXINPUT_GAMEPAD = *mut XINPUT_GAMEPAD;

//Device button	Bitmask
pub mod XINPUT_GAMEPAD_CONSTS {
	use types::WORD;

	pub const DPAD_UP: WORD = 0x0001;
	pub const DPAD_DOWN: WORD = 0x0002;
	pub const DPAD_LEFT: WORD = 0x0004;
	pub const DPAD_RIGHT: WORD = 0x0008;
	pub const START: WORD = 0x0010;
	pub const BACK: WORD = 0x0020;
	pub const LEFT_THUMB: WORD = 0x0040;
	pub const RIGHT_THUMB: WORD = 0x0080;
	pub const LEFT_SHOULDER: WORD = 0x0100;
	pub const RIGHT_SHOULDER: WORD = 0x0200;
	pub const A: WORD = 0x1000;
	pub const B: WORD = 0x2000;
	pub const X: WORD = 0x4000;
	pub const Y: WORD = 0x8000;
}

// Negative values signify down or to the left.
// Positive values signify up or to the right.
// A value of 0 is centered.
pub const XINPUT_GAMEPAD_THUMB_X_MIN: DWORD = -32768;
pub const XINPUT_GAMEPAD_THUMB_X_MAX: DWORD = 32767;

//The constants XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE or XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE can be used as a positive and negative value to filter a thumbstick input.
pub const XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE: DWORD = 7849;
pub const XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE: DWORD = 8689;


pub const XINPUT_GAMEPAD_TRIGGER_MIN: DWORD = 0;
pub const XINPUT_GAMEPAD_TRIGGER_MAX: DWORD = 255;

pub const XINPUT_GAMEPAD_TRIGGER_THRESHOLD: DWORD = 30;

#[repr(C)]
pub struct XINPUT_KEYSTROKE {
	pub VirtualKey: WORD,
	pub Unicode: WCHAR,
	pub Flags: WORD,
	pub UserIndex: BYTE,
	pub HidCode: BYTE,
}
impl Copy for XINPUT_KEYSTROKE {}
pub type PXINPUT_KEYSTROKE = *mut XINPUT_KEYSTROKE;

pub mod XINPUT_KEYSTROKE {
	use types::DWORD;

	pub const KEYDOWN: DWORD = 0x0001;
	pub const KEYUP: DWORD = 0x0002;
	pub const REPEAT: DWORD = 0x0004;
}

pub mod VK_PAD {
	use types::DWORD;

	pub const A: DWORD = 0x5800;
	pub const B: DWORD = 0x5801;
	pub const X: DWORD = 0x5802;
	pub const Y: DWORD = 0x5803;
	pub const RSHOULDER: DWORD = 0x5804;
	pub const LSHOULDER: DWORD = 0x5805;
	pub const LTRIGGER: DWORD = 0x5806;
	pub const RTRIGGER: DWORD = 0x5807;

	pub const DPAD_UP: DWORD = 0x5810;
	pub const DPAD_DOWN: DWORD = 0x5811;
	pub const DPAD_LEFT: DWORD = 0x5812;
	pub const DPAD_RIGHT: DWORD = 0x5813;
	pub const START: DWORD = 0x5814;
	pub const BACK: DWORD = 0x5815;
	pub const LTHUMB_PRESS: DWORD = 0x5816;
	pub const RTHUMB_PRESS: DWORD = 0x5817;

	pub const LTHUMB_UP: DWORD = 0x5820;
	pub const LTHUMB_DOWN: DWORD = 0x5821;
	pub const LTHUMB_RIGHT: DWORD = 0x5822;
	pub const LTHUMB_LEFT: DWORD = 0x5823;
	pub const LTHUMB_UPLEFT: DWORD = 0x5824;
	pub const LTHUMB_UPRIGHT: DWORD = 0x5825;
	pub const LTHUMB_DOWNRIGHT: DWORD = 0x5826;
	pub const LTHUMB_DOWNLEFT: DWORD = 0x5827;

	pub const RTHUMB_UP: DWORD = 0x5830;
	pub const RTHUMB_DOWN: DWORD = 0x5831;
	pub const RTHUMB_RIGHT: DWORD = 0x5832;
	pub const RTHUMB_LEFT: DWORD = 0x5833;
	pub const RTHUMB_UPLEFT: DWORD = 0x5834;
	pub const RTHUMB_UPRIGHT: DWORD = 0x5835;
	pub const RTHUMB_DOWNRIGHT: DWORD = 0x5836;
	pub const RTHUMB_DOWNLEFT: DWORD = 0x5837;
}

#[repr(C)]
pub struct XINPUT_STATE {
	pub dwPacketNumber: DWORD,
	pub Gamepad: XINPUT_GAMEPAD,
}
impl Copy for XINPUT_STATE {}
pub type PXINPUT_STATE = *mut XINPUT_STATE;

#[repr(C)]
pub struct XINPUT_VIBRATION {
	pub wLeftMotorSpeed: WORD,
	pub wRightMotorSpeed: WORD,
}
impl Copy for XINPUT_VIBRATION {}
pub type PXINPUT_VIBRATION = *mut XINPUT_VIBRATION;

pub const XINPUT_GAMEPAD_MOTOR_MIN: DWORD = 0;
pub const XINPUT_GAMEPAD_MOTOR_MAX: DWORD = 65_535;

// Change depending on platform, BUT reccomend actually Dynamic Loading and using LoadLibrary and the function ptr's in dll mod!
#[cfg(feature="static_link")]
#[link(name = "xinput1_4")]
extern "stdcall" {
	pub fn XInputEnable(enable: BOOL);

	pub fn XInputGetAudioDeviceIds(dwUserIndex: DWORD, pRenderDeviceId: LPWSTR, pRenderCount: *mut UINT, pCaptureDeviceId: LPWSTR, pCaptureCount: *mut UINT) -> DWORD;

	pub fn XInputGetBatteryInformation(dwUserIndex: DWORD, devType: BYTE, pBatteryInformation: *mut XINPUT_BATTERY_INFORMATION) -> DWORD;

	pub fn XInputGetCapabilities(dwUserIndex: DWORD, dwFlags: DWORD, pCapabilities: *mut XINPUT_CAPABILITIES) -> DWORD;

	pub fn XInputGetDSoundAudioDeviceGuids(dwUserIndex: DWORD, pDSoundRenderGuid: *mut GUID, pDSoundCaptureGuid: *mut GUID) -> DWORD;

	pub fn XInputGetKeystroke(dwUserIndex: DWORD, dwReserved: DWORD, pKeystroke: PXINPUT_KEYSTROKE) -> DWORD;

	pub fn XInputGetState(dwUserIndex: DWORD, pState: *mut XINPUT_STATE) -> DWORD;

	pub fn XInputSetState(dwUserIndex: DWORD, pVibration: *mut XINPUT_VIBRATION) -> DWORD;
}